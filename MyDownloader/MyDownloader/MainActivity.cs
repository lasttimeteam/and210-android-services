﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace MyDownloader
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            FindViewById<Button>(Resource.Id.startDownloadButton).Click += StartDownloadButtonClicked;
            FindViewById<Button>(Resource.Id.cancelDownloadButton).Click += CancelDownloadButtonClicked;
        }

        private void StartDownloadButtonClicked(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(MyDowloadService));
            intent.PutExtra("LoopCount", 70);

            StartService(intent);
        }

        private void CancelDownloadButtonClicked(object sender, EventArgs e)
        {
            StopService(new Intent(this, typeof(MyDowloadService)));
        }
    }
}