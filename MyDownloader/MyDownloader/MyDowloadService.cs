﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Widget;
using Java.Lang;

namespace MyDownloader
{
    [Service(Label="MyDowloadService", Icon="@mipmap/ic_launcher")]
    public class MyDowloadService : Service
    {
        private const string tag = "MyDownloadService";

        private volatile bool isCancelled;
        private volatile bool isDownloaded;

        private PendingIntent pendingIntent;

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug(tag, "Service Bound");

            throw new NotImplementedException();
        }

        public override void OnCreate()
        {
            Log.Debug(tag, "Service created");
            base.OnCreate();

            pendingIntent = PendingIntent.GetActivity(this, 0, new Intent(this, typeof(MainActivity)), 0);
        }

        private const int NotificationId = 12000;

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Log.Debug(tag, "Service started");

            isCancelled = false;
            isDownloaded = false;

            StartForeground(NotificationId, GetNotification("Download started"));

            Toast.MakeText(this, "Download Started", ToastLength.Short).Show();

            int steps = intent.GetIntExtra("LoopCount", 10);

            Task.Run(() =>
            {
                for (int i = 0; i < steps && isCancelled == false; i++)
                {
                    int percent = 100 * (i + 1) / steps;

                    var msg = $"{startId} download in progress: {percent} complete";

                    Log.Debug(tag, msg);

                    UpdateNotification(msg);

                    Thread.Sleep(500);
                }

                if (isCancelled) return;
                isDownloaded = true;
                StopSelf();
            });

            return StartCommandResult.RedeliverIntent;
        }

        public override void OnDestroy()
        {
            isCancelled = true;

            if (isDownloaded)
            {
                Toast.MakeText(this, "Download Complete", ToastLength.Short).Show();
            }
            else
            {
                Toast.MakeText(this, "Download Cancelled", ToastLength.Short).Show();
            }

            Log.Debug(tag, "Service destroyed");
            base.OnDestroy();
        }

        Notification GetNotification(string content)
        {
            return new Notification.Builder(this)
                .SetContentTitle(tag)
                .SetContentText(content)
                .SetSmallIcon(Resource.Mipmap.ic_launcher)
                .SetContentIntent(pendingIntent)
                .Build();
        }

        void UpdateNotification(string content)
        {
            var notification = GetNotification(content);

           var notMgr = (NotificationManager)GetSystemService(Context.NotificationService);
           notMgr.Notify(NotificationId, notification);
        }
    }
}